# reto-clm

## La app
![NodeJs](./img/nodejs.png)

```bash
$ git clone https://gitlab.com/angeelbert/reto-clm.git

$ cd ./reto-clm

```

### Instalar Dependencias
```bash
$ npm install

```

### Ejecutar Test
![Jest](./img/jest.jpg)

```bash
$ npm run test

```

### Crear imagen Docker
![docker](./img/nodedoker.jpg)

```bash
$ docker build -t reto-clm .

```

### Correr imagen Docker

```bash
$ docker run -p 3000:3000 reto-clm 

```

### Correr Docker-Compose
![compose](./img/docker-compose.png)

```bash
$ docker-compose up

```
### Nginx (user/pass angeelbert/Starwars)